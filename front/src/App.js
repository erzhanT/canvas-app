import React, {useEffect, useRef, useState} from 'react';

const App = () => {

    const [name, setName] = useState('');
    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: [],
        username: ''
    });

    const ws = useRef(null);
    const canvas = useRef(null);

    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/pixels');

        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);
            if(decoded.type === 'CONNECTED') {
                setName(decoded.user)
            }

            let data = decoded.newPixels

            if (decoded.type === 'NEW_PIXEL') {
                for (let item of data) {

                    const context = canvas.current.getContext('2d');
                    const imageData = context.createImageData(1, 1);
                    const d = imageData.data;

                    d[0] = 0;
                    d[1] = 0;
                    d[2] = 0;
                    d[3] = 255;

                    context.putImageData(imageData, item.x, item.y);

                }

            }
        };

    }, []);


    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            event.persist();
            const clientX = event.clientX;
            const clientY = event.clientY;
            setState(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY
                    }]
                };
            });

            const context = canvas.current.getContext('2d');
            const imageData = context.createImageData(1, 1);
            const d = imageData.data;

            d[0] = 0;
            d[1] = 0;
            d[2] = 0;
            d[3] = 255;

            context.beginPath();
            context.putImageData(imageData, event.clientX, event.clientY);
            context.fill();
        }
    };

    const mouseDownHandler = event => {
        setState({...state, mouseDown: true});
    };

    const mouseUpHandler = event => {
        ws.current.send(JSON.stringify({
            type: "CREATE_PIXELS",
            newPixels: state.pixelsArray
        }));
        setState({...state, mouseDown: false, pixelsArray: []});
    };

    return (
        <div className='App'>
            <h4>{name} connected!</h4>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};
export default App;
