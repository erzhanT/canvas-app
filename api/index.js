const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid')
const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};
let pixels = [];

app.ws('/pixels', (ws, req) => {

    const id = nanoid();
    console.log('Client connected! id=', id);
    activeConnections[id] = ws;
    const user = 'User ' + id

    ws.send(JSON.stringify({type: 'CONNECTED', user}))

    ws.on('message', (msg) => {
        const decoded = JSON.parse(msg);

        if (decoded.type === 'CREATE_PIXELS') {
            Object.keys(activeConnections).forEach(key => {
                const connection = activeConnections[key];
                pixels = pixels.concat(decoded.newPixels)
                connection.send(JSON.stringify({
                    type: 'NEW_PIXEL',
                    newPixels: decoded.newPixels,
                }));
            });
        }
    });

    ws.on('close', () => {
        console.log('Client disconnected! id =', id);
        delete activeConnections[id];
    });

});

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});